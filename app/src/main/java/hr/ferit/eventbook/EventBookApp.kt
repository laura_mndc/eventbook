package hr.ferit.eventbook

import android.app.Application
import hr.ferit.eventbook.di.databaseModule
import hr.ferit.eventbook.di.repositoryModule
import hr.ferit.eventbook.di.viewmodelModule

import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class EventBookApp : Application() {
    override fun onCreate() {
        super.onCreate()
        application = this

        startKoin {
            androidLogger(if (BuildConfig.DEBUG) Level.ERROR else Level.NONE)
            androidContext(this@EventBookApp)
            modules(
                databaseModule,
                repositoryModule,
                viewmodelModule
            )
        }

    }

    companion object {
        lateinit var application: Application
    }
}