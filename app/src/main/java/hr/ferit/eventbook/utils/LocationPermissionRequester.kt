package hr.ferit.eventbook.utils

import android.Manifest
import androidx.fragment.app.Fragment
import hr.ferit.eventbook.R

class LocationPermissionRequester(
    fragment: Fragment,
    onGranted: () -> Unit,
    onDismissed: () -> Unit,
) : BasePermissionRequester(fragment, onGranted, onDismissed) {
    override val permissions: Array<String> = arrayOf(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION
    )
    override val titleResId = R.string.android_permission_location_title
    override val descriptionResId = R.string.android_permission_location_description
    override val descriptionWhenDeniedResId =
        R.string.android_permission_location_denied_description
}