package hr.ferit.eventbook.utils

enum class DayOptions {
    YESTERDAY, TODAY, TOMORROW
}