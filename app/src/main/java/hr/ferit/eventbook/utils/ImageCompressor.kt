package hr.ferit.eventbook.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.InputStream

class ImageCompressor {
    companion object {
        private fun getFileSize(context: Context, imageUri: Uri): Int? {

            try {
                val connInputStream: InputStream? =
                    context.contentResolver.openInputStream(imageUri)
                return connInputStream?.available()

            } catch (e: IOException) {
                e.printStackTrace()
            }
            return null

        }

        private fun determineQuality(size: Int?): Int {
            return if (size == null) {
                50
            } else if (size < 1000000) {
                100
            } else if (size < 2000000) {
                50
            } else if (size < 3000000) {
                33
            } else if (size < 4000000) {
                25
            } else {
                10
            }
        }

        fun compress(context: Context?, imageUri: Uri): ByteArray? {
            context?.let {
                determineQuality(getFileSize(context, imageUri))
                val bitmap = when {
                    Build.VERSION.SDK_INT < 28 -> MediaStore.Images.Media.getBitmap(
                        context.contentResolver,
                        imageUri
                    )
                    else -> {
                        val source = ImageDecoder.createSource(context.contentResolver, imageUri)
                        ImageDecoder.decodeBitmap(source)
                    }
                }
                val baos = ByteArrayOutputStream()
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos)
                return baos.toByteArray()
            }
            return null
        }
    }
}