import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Environment
import android.widget.Toast
import com.squareup.picasso.Picasso.LoadedFrom
import com.squareup.picasso.Target
import hr.ferit.eventbook.R
import java.io.File
import java.io.FileOutputStream
import java.util.*

class ImageDownloader(val context: Context?) : Target {
    override fun onPrepareLoad(arg0: Drawable?) {
        Toast.makeText(context, context?.getString(R.string.toast_saving_image), Toast.LENGTH_SHORT)
            .show()
    }

    override fun onBitmapLoaded(bitmap: Bitmap, arg1: LoadedFrom) {
        val fileName = UUID.randomUUID().toString()
        val file =
            File(Environment.getExternalStorageDirectory().path + "/" + Environment.DIRECTORY_DOWNLOADS + "/${fileName}.jpg")
        try {
            file.createNewFile()
            val ostream = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 75, ostream)
            ostream.close()
            Toast.makeText(
                context,
                context?.getString(R.string.toast_saved_image),
                Toast.LENGTH_SHORT
            ).show()

        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(
                context,
                context?.getString(R.string.toast_error_download),
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    override fun onBitmapFailed(e: java.lang.Exception?, errorDrawable: Drawable?) {
        Toast.makeText(
            context,
            context?.getString(R.string.toast_error_download),
            Toast.LENGTH_SHORT
        ).show()
    }

}