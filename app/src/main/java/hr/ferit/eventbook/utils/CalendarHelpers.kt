package hr.ferit.eventbook.utils


import java.util.*

fun getTodayDate(): Calendar {
    val calendar = Calendar.getInstance()
    calendar[Calendar.SECOND] = 0
    calendar[Calendar.MINUTE] = 0
    calendar[Calendar.HOUR_OF_DAY] = 0
    return calendar
}