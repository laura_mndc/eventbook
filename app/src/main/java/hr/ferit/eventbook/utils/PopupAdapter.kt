package hr.ferit.eventbook.utils

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter
import com.google.android.gms.maps.model.Marker


internal class PopupAdapter(val context: Context?) : InfoWindowAdapter {
    override fun getInfoWindow(arg0: Marker): View? {
        return null
    }

    override fun getInfoContents(marker: Marker): View {
        val info = LinearLayout(context)
        info.orientation = LinearLayout.VERTICAL
        val title = TextView(context)
        title.setTextColor(Color.BLACK)
        title.gravity = Gravity.CENTER
        title.setTypeface(null, Typeface.BOLD)
        title.text = marker.title
        val snippet = TextView(context)
        snippet.setTextColor(Color.GRAY)
        snippet.text = marker.snippet
        info.addView(title)
        info.addView(snippet)
        return info
    }
}