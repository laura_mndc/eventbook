package hr.ferit.eventbook.data

import com.google.firebase.auth.FirebaseUser


interface AuthRepository {
    val currentUser: FirebaseUser?

    suspend fun firebaseSignUpWithEmailAndPassword(email: String, password: String): Result<Boolean>

    suspend fun sendEmailVerification(): Result<Boolean>

    suspend fun firebaseSignInWithEmailAndPassword(email: String, password: String): Result<Boolean>

    suspend fun sendPasswordResetEmail(email: String): Result<Boolean>

    fun signOut()

}