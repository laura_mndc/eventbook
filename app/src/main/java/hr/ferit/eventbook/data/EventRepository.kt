package hr.ferit.eventbook.data


import android.net.Uri
import com.google.firebase.firestore.DocumentReference
import hr.ferit.eventbook.model.Event
import hr.ferit.eventbook.model.Location
import hr.ferit.eventbook.model.Photo
import kotlinx.coroutines.flow.Flow
import java.util.*


interface EventRepository {
    fun getEventsFromFirestore(): Flow<Result<List<Event>>>
    fun getEventsByDateFromFirestore(
        startTime: Calendar,
        endTime: Calendar
    ): Flow<Result<List<Event>>>

    fun getFilteredEventsFromFirestore(query: String): Flow<Result<List<Event>>>
    fun getEventDetailsById(id: String): Flow<Result<Event?>>
    suspend fun addEventToFirestore(
        title: String,
        location: Location,
        date: Date,
        coverImageUrl: Uri?,
        authorId: String?
    ): Result<Boolean>

    fun getEventImagesFromFirestore(eventId: String): Flow<Result<List<Photo>>>
    suspend fun addPhotoToFirestore(
        authorId: String?,
        eventId: String,
        imageUrl: Uri?
    ): Result<Boolean>

    fun getLocationsFromFirestore(): Flow<Result<List<Location>>>
    fun getLocationByReference(ref: DocumentReference): Flow<Result<Location?>>

}

