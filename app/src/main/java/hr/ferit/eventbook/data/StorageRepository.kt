package hr.ferit.eventbook.data

import android.net.Uri


interface StorageRepository {
    suspend fun addImageToFirebaseStorage(image: ByteArray): Result<Uri>
    suspend fun addImageToFirebaseStorage(image: Uri): Result<Uri>
}