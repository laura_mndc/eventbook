package hr.ferit.eventbook.data

import android.net.Uri
import com.google.firebase.firestore.*
import hr.ferit.eventbook.model.Event
import hr.ferit.eventbook.model.Location
import hr.ferit.eventbook.model.Photo
import hr.ferit.eventbook.model.binding_models.PhotoBM
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.tasks.await
import java.util.*
import hr.ferit.eventbook.utils.getTodayDate
import kotlinx.coroutines.flow.Flow

class EventRepositoryImpl(
    private val rootRef: FirebaseFirestore = FirebaseFirestore.getInstance(),
    private val eventRef: CollectionReference = rootRef.collection("events")

) : EventRepository {
    override fun getEventsFromFirestore() = callbackFlow {
        val snapshotListener = eventRef.orderBy("dateOfEvent", Query.Direction.DESCENDING).limit(10)
            .addSnapshotListener { snapshot, e ->
                val eventsResponse = if (snapshot != null) {
                    val events = snapshot.toObjects(Event::class.java)
                    Result.success(events.filterNotNull())
                } else {
                    Result.failure(e ?: Exception("Unknown exception."))
                }
                trySend(eventsResponse)
            }
        awaitClose {
            snapshotListener.remove()
        }
    }

    override fun getEventsByDateFromFirestore(startTime: Calendar, endTime: Calendar) =
        callbackFlow {

            val snapshotListener = eventRef.whereGreaterThanOrEqualTo("dateOfEvent", startTime.time)
                .whereLessThan("dateOfEvent", endTime.time)
                .addSnapshotListener { snapshot, e ->
                    val eventsResponse = if (snapshot != null) {
                        val events = snapshot.toObjects(Event::class.java)
                        Result.success(events.filterNotNull())
                    } else {
                        Result.failure(e ?: Exception("Unknown exception."))
                    }
                    trySend(eventsResponse)
                }
            awaitClose {
                snapshotListener.remove()
            }
        }

    override fun getFilteredEventsFromFirestore(query: String) = callbackFlow {
        val snapshotListener =
            eventRef.orderBy("title").startAt(query).endAt(query + "\uf8ff").limit(10)
                .addSnapshotListener { snapshot, e ->
                    val eventsResponse = if (snapshot != null) {
                        val events = snapshot.toObjects(Event::class.java)
                        Result.success(events.filterNotNull())
                    } else {
                        Result.failure(e ?: Exception("Unknown exception."))
                    }
                    trySend(eventsResponse)
                }
        awaitClose {
            snapshotListener.remove()
        }
    }

    override fun getLocationsFromFirestore() = callbackFlow {
        val snapshotListener =
            rootRef.collection("locations").orderBy("name").addSnapshotListener { snapshot, e ->
                val locationsResponse = if (snapshot != null) {
                    val events = snapshot.toObjects(Location::class.java)
                    Result.success(events.filterNotNull())
                } else {
                    Result.failure(e ?: Exception("Unknown exception."))
                }
                trySend(locationsResponse)
            }
        awaitClose {
            snapshotListener.remove()
        }
    }

    override fun getEventImagesFromFirestore(eventId: String) = callbackFlow {
        val reference = eventRef.document(eventId)
        val snapshotListener = rootRef.collection("photos").whereEqualTo("eventId", reference)
            .orderBy("timeUploaded").addSnapshotListener { snapshot, e ->
                val photosResponse = if (snapshot != null) {
                    val photos = snapshot.toObjects(Photo::class.java)
                    Result.success(photos.filterNotNull())
                } else {
                    Result.failure(e ?: Exception("Unknown exception."))
                }
                trySend(photosResponse)
            }
        awaitClose {
            snapshotListener.remove()
        }
    }


    override fun getEventDetailsById(id: String) = callbackFlow {
        val snapshotListener = eventRef.document(id).addSnapshotListener { snapshot, e ->
            val eventResponse = if (snapshot != null) {
                val event = snapshot.toObject(Event::class.java)
                Result.success(event)
            } else {
                Result.failure(e ?: Exception("Unknown exception."))
            }
            trySend(eventResponse)
        }
        awaitClose {
            snapshotListener.remove()
        }
    }

    override fun getLocationByReference(ref: DocumentReference) = callbackFlow {
        val snapshotListener = ref.addSnapshotListener { snapshot, e ->
            val locationResponse = if (snapshot != null) {
                val location = snapshot.toObject(Location::class.java)
                Result.success(location)
            } else {
                Result.failure(e ?: Exception("Unknown exception."))
            }
            trySend(locationResponse)
        }
        awaitClose {
            snapshotListener.remove()
        }
    }

    override suspend fun addEventToFirestore(
        title: String,
        location: Location,
        date: Date,
        coverImageUrl: Uri?,
        authorId: String?
    ): Result<Boolean> {
        return try {

            var locationRef = rootRef.collection("locations")
                .document(location.id ?: UUID.randomUUID().toString())
            var userRef = authorId?.let { rootRef.collection("users").document(it) }
            var event = Event(
                title = title,
                locationId = locationRef,
                dateAdded = Date(),
                dateOfEvent = date,
                coverImageUri = coverImageUrl.toString(),
                authorId = userRef
            )
            locationRef.set(location).await()
            eventRef.document().set(event).await()
            Result.success(true)
        } catch (e: Exception) {
            Result.failure(e)
        }
    }

    override suspend fun addPhotoToFirestore(
        authorId: String?,
        eventId: String,
        imageUrl: Uri?
    ): Result<Boolean> {
        return try {
            var selectedEventRef = rootRef.document("events/${eventId}")
            var userRef = authorId?.let { rootRef.collection("users").document(it) }
            var photo = PhotoBM(
                authorId = userRef,
                eventId = selectedEventRef,
                timeUploaded = FieldValue.serverTimestamp(),
                uri = imageUrl.toString()
            )
            rootRef.collection("photos").document().set(photo).await()
            Result.success(true)
        } catch (e: Exception) {
            Result.failure(e)
        }
    }
}