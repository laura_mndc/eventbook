package hr.ferit.eventbook.data


import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import hr.ferit.eventbook.model.User
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.tasks.await


class UserRepositoryImpl(
    private val rootRef: FirebaseFirestore = FirebaseFirestore.getInstance(),
    private val userRef: CollectionReference = rootRef.collection("users")
) : UserRepository {
    override fun getUserDetailsById(id: String) = callbackFlow {
        val snapshotListener = userRef.document(id).addSnapshotListener { snapshot, e ->
            val userResponse = if (snapshot != null) {
                val user = snapshot.toObject(User::class.java)
                Result.success(user)
            } else {
                Result.failure(e ?: Exception("Unknown exception."))
            }
            trySend(userResponse)
        }
        awaitClose {
            snapshotListener.remove()
        }
    }

    override suspend fun addUserToFirestore(user: User): Result<Boolean> {
        return try {
            if (user.id != null) {
                userRef.document(user.id!!).set(user).await()
            } else {
                userRef.document().set(user).await()
            }
            Result.success(true)
        } catch (e: Exception) {
            Result.failure(e)
        }
    }
}