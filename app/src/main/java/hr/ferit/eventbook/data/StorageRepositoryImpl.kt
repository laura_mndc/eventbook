package hr.ferit.eventbook.data


import android.net.Uri
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.ktx.storage
import kotlinx.coroutines.tasks.await
import java.util.*


class StorageRepositoryImpl(
    private val storage: FirebaseStorage = Firebase.storage
) : StorageRepository {

    override suspend fun addImageToFirebaseStorage(image: ByteArray): Result<Uri> {
        return try {
            val fileName = UUID.randomUUID().toString()
            val downloadUrl = storage.reference.child("file/$fileName")
                .putBytes(image).await()
                .storage.downloadUrl.await()
            Result.success(downloadUrl)
        } catch (e: Exception) {
            Result.failure(e)
        }
    }

    override suspend fun addImageToFirebaseStorage(image: Uri): Result<Uri> {
        return try {
            val fileName = UUID.randomUUID().toString()
            val downloadUrl = storage.reference.child("file/$fileName")
                .putFile(image).await()
                .storage.downloadUrl.await()
            Result.success(downloadUrl)
        } catch (e: Exception) {
            Result.failure(e)
        }
    }


}