package hr.ferit.eventbook.data

import hr.ferit.eventbook.model.User
import kotlinx.coroutines.flow.Flow

interface UserRepository {
    fun getUserDetailsById(id: String): Flow<Result<User?>>
    suspend fun addUserToFirestore(user: User): Result<Boolean>
}