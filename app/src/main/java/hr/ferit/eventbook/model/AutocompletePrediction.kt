package hr.ferit.eventbook.model

data class AutocompletePrediction(
    var placeId: String? = null,
    val primaryText: String? = null,
    val secondaryText: String? = null

)