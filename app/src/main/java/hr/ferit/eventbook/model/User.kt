package hr.ferit.eventbook.model

import com.google.firebase.firestore.DocumentId

data class User(
    @DocumentId
    var id: String? = null,
    val firstName: String? = null,
    val lastName: String? = null,
    val email: String? = null
)