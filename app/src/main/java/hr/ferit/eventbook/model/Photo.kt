package hr.ferit.eventbook.model

import com.google.firebase.firestore.DocumentId
import com.google.firebase.firestore.DocumentReference
import java.util.Date


data class Photo(
    @DocumentId
    var id: String? = null,
    val uri: String? = null,
    val timeUploaded: Date? = null,
    val authorId: DocumentReference? = null,
    val eventId: DocumentReference? = null
)