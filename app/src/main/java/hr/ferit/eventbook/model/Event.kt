package hr.ferit.eventbook.model


import com.google.firebase.firestore.DocumentId
import com.google.firebase.firestore.DocumentReference
import java.util.*


data class Event(
    @DocumentId
    var id: String? = null,
    val title: String = "",
    val locationId: DocumentReference? = null,
    val coverImageUri: String? = null,
    val dateAdded: Date? = null,
    val dateOfEvent: Date? = null,
    val authorId: DocumentReference? = null
)