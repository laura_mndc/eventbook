package hr.ferit.eventbook.model.binding_models

import com.google.firebase.firestore.DocumentId
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FieldValue

data class PhotoBM(
    @DocumentId
    var id: String? = null,
    val uri: String? = null,
    val timeUploaded: FieldValue? = null,
    val authorId: DocumentReference? = null,
    val eventId: DocumentReference? = null
) {
}