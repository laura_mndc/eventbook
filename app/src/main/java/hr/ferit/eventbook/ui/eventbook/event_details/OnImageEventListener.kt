package hr.ferit.eventbook.ui.eventbook.event_details

interface OnImageEventListener {
    fun onImageSelected(imageUri: String?)
}