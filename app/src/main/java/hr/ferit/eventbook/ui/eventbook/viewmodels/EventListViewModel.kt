package hr.ferit.eventbook.ui.eventbook.viewmodels

import androidx.lifecycle.ViewModel
import hr.ferit.eventbook.data.EventRepository


class EventListViewModel(
    val eventRepository: EventRepository
) : ViewModel() {
    fun getEvents() = eventRepository.getEventsFromFirestore()
    fun getFilteredEvents(query: String) = eventRepository.getFilteredEventsFromFirestore(query)
    fun getLocations() = eventRepository.getLocationsFromFirestore()

}