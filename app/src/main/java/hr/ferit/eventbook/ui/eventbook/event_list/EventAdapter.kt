package hr.ferit.eventbook.ui.eventbook.event_list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import hr.ferit.eventbook.R
import hr.ferit.eventbook.model.Event
import hr.ferit.eventbook.model.Location

class EventAdapter : RecyclerView.Adapter<EventViewHolder>() {

    private val events = mutableListOf<Event>()
    private val locations = mutableListOf<Location>()
    private var eventsFiltered = mutableListOf<Event>()
    var onEventSelectedListener: OnEventEventListener? = null

    fun setEvents(events: List<Event>) {
        this.events.clear()
        this.events.addAll(events)
        this.eventsFiltered.clear()
        this.eventsFiltered.addAll(events)
        this.notifyDataSetChanged()
    }

    fun setFilteredEvents(events: List<Event>) {
        this.eventsFiltered.clear()
        this.eventsFiltered.addAll(events)
        this.notifyDataSetChanged()
    }

    fun resetEvents() {
        this.eventsFiltered.clear()
        this.eventsFiltered.addAll(events)
        this.notifyDataSetChanged()
    }

    fun setLocations(locations: List<Location>) {
        this.locations.clear()
        this.locations.addAll(locations)
        this.notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_event, parent, false)
        return EventViewHolder(view)
    }

    override fun onBindViewHolder(holder: EventViewHolder, position: Int) {
        val event = eventsFiltered[position]
        val locationName = locations.find { it.id == event.locationId?.id }?.name
        holder.bind(event, locationName ?: "")
        onEventSelectedListener?.let { listener ->
            holder.itemView.setOnClickListener { listener.onEventSelected(event.id) }

        }
    }

    override fun getItemCount(): Int = eventsFiltered.count()


}