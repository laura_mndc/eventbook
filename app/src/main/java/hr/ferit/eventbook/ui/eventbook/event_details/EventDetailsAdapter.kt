package hr.ferit.eventbook.ui.eventbook.event_details

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import hr.ferit.eventbook.R
import hr.ferit.eventbook.model.Event
import hr.ferit.eventbook.model.Location
import hr.ferit.eventbook.model.Photo
import hr.ferit.eventbook.ui.eventbook.event_list.EventViewHolder
import hr.ferit.eventbook.ui.eventbook.event_list.OnEventEventListener

class EventDetailsAdapter : RecyclerView.Adapter<EventImageViewHolder>() {

    private val images = mutableListOf<Photo>()

    var onImageSelectedListener: OnImageEventListener? = null

    fun setImages(events: List<Photo>) {
        this.images.clear()
        this.images.addAll(events)
        this.notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventImageViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_image, parent, false)
        return EventImageViewHolder(view)
    }

    override fun onBindViewHolder(holder: EventImageViewHolder, position: Int) {
        val image = images[position]

        holder.bind(image)
        onImageSelectedListener?.let { listener ->
            holder.itemView.setOnClickListener { listener.onImageSelected(image.uri) }

        }
    }

    override fun getItemCount(): Int = images.count()
}