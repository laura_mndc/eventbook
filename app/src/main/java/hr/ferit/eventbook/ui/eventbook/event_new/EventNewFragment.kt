package hr.ferit.eventbook.ui.eventbook.event_new


import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.coroutineScope
import androidx.navigation.fragment.findNavController
import hr.ferit.eventbook.R
import hr.ferit.eventbook.databinding.FragmentEventNewBinding
import hr.ferit.eventbook.model.Location
import hr.ferit.eventbook.ui.eventbook.viewmodels.EventNewViewModel
import hr.ferit.eventbook.utils.ImageCompressor
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.activityViewModel
import java.util.*


class EventNewFragment : Fragment() {

    private lateinit var binding: FragmentEventNewBinding
    private var selectedLocation: Location? = null
    private var imageUri: Uri? = null
    private val viewModel: EventNewViewModel by activityViewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentEventNewBinding.inflate(layoutInflater)
        viewModel.selectedLocation.observe(viewLifecycleOwner) { location ->
            selectedLocation = location
            binding.tvEventLocationInput.text = location.name

        }

        binding.bSaveEvent.setOnClickListener { saveEvent() }
        binding.tpEventTime.setIs24HourView(true)

        val selectImageIntent = registerForActivityResult(ActivityResultContracts.GetContent())
        { uri ->
            binding.ivCoverImage.setImageURI(uri)
            imageUri = uri
        }
        binding.bSelectImage.setOnClickListener { selectImageIntent.launch(getString(R.string.image_intent)) }
        binding.tvEventLocationInput.setOnClickListener {
            onClickLocations()
        }
        return binding.root
    }

    private fun getTimeFromBinding(): Date {
        val cal: Calendar = Calendar.getInstance()
        cal.set(Calendar.DAY_OF_MONTH, binding.dpEventDate.dayOfMonth)
        cal.set(Calendar.MONTH, binding.dpEventDate.month)
        cal.set(Calendar.YEAR, binding.dpEventDate.year)
        cal.set(Calendar.HOUR_OF_DAY, binding.tpEventTime.hour)
        cal.set(Calendar.MINUTE, binding.tpEventTime.minute)
        cal.set(Calendar.SECOND, 0)
        return cal.time
    }

    private fun saveEvent() {
        val title = binding.etEventTitleInput.text.toString()

        if (selectedLocation != null) {
            lifecycle.coroutineScope.launch(Dispatchers.IO) {
                imageUri?.let {
                    val compressedImage = ImageCompressor.compress(requireContext(), it)
                    if (compressedImage != null) {
                        viewModel.save(
                            title,
                            selectedLocation!!,
                            getTimeFromBinding(),
                            compressedImage
                        )
                    } else {
                        viewModel.save(title, selectedLocation!!, getTimeFromBinding(), it)
                    }
                }
                lifecycle.coroutineScope.launch(Dispatchers.Main) {
                    Toast.makeText(
                        context,
                        getString(R.string.fragment_new_toast_saved),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
            Toast.makeText(
                context,
                getString(R.string.fragment_new_toast_saving),
                Toast.LENGTH_SHORT
            ).show()
            val action = EventNewFragmentDirections.actionEventNewFragmentToEventListFragment()
            findNavController().navigate(action)
        } else {
            Toast.makeText(
                context,
                getString(R.string.fragment_new_toast_location_missing),
                Toast.LENGTH_SHORT
            ).show()
        }

    }


    private fun onClickLocations() {
        var fragmentManager = (binding.root.context as FragmentActivity).supportFragmentManager

        var fragment = EventLocationsDialogFragment()
        fragment.show(fragmentManager, EventLocationsDialogFragment.TAG)


    }


    companion object {
        val Tag = "NewEvent"

        fun create(): Fragment {
            return EventNewFragment()
        }
    }


}