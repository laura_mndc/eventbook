package hr.ferit.eventbook.ui.eventbook.event_new

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import hr.ferit.eventbook.databinding.ItemLocationBinding
import hr.ferit.eventbook.model.AutocompletePrediction


class LocationViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(location: AutocompletePrediction) {
        val binding = ItemLocationBinding.bind(itemView)
        binding.tvLocationName.text = location.primaryText
        binding.tvLocationSecondaryText.text = location.secondaryText
    }


}