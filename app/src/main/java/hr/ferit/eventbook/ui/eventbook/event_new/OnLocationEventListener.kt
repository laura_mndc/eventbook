package hr.ferit.eventbook.ui.eventbook.event_new

import hr.ferit.eventbook.model.AutocompletePrediction

interface OnLocationEventListener {
    fun onLocationSelected(location: AutocompletePrediction)
}