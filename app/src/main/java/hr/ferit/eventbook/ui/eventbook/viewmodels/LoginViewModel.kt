package hr.ferit.eventbook.ui.eventbook.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import hr.ferit.eventbook.data.AuthRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class LoginViewModel(
    private val repo: AuthRepository
) : ViewModel() {

    var signInResponse = MutableLiveData<Result<Boolean>>()


    fun signInWithEmailAndPassword(email: String, password: String) =
        viewModelScope.launch(Dispatchers.IO) {
            var result = repo.firebaseSignInWithEmailAndPassword(email, password)
            viewModelScope.launch(Dispatchers.Main) { signInResponse.value = result }
        }

    fun sendPasswordResetEmail(email: String) = viewModelScope.launch(Dispatchers.IO) {
        repo.sendPasswordResetEmail(email)
    }


}