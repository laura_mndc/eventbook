package hr.ferit.eventbook.ui.eventbook.events_map


import android.annotation.SuppressLint
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.coroutineScope
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.OnCameraIdleListener
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import hr.ferit.eventbook.R
import hr.ferit.eventbook.databinding.FragmentEventsMapBinding
import hr.ferit.eventbook.model.Event
import hr.ferit.eventbook.ui.eventbook.viewmodels.EventMapViewModel
import hr.ferit.eventbook.utils.DayOptions
import hr.ferit.eventbook.utils.LocationPermissionRequester
import hr.ferit.eventbook.utils.PopupAdapter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.joinAll
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel


class EventsMapFragment : Fragment(), OnMapReadyCallback, OnCameraIdleListener {

    private lateinit var binding: FragmentEventsMapBinding
    private val viewModel: EventMapViewModel by viewModel()
    private var map: GoogleMap? = null
    private var cameraPosition: CameraPosition? = null
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var mapFragment: SupportMapFragment
    private val defaultLocation = Location(LocationManager.NETWORK_PROVIDER)
    private var locationPermissionGranted = false
    private var allLocations = listOf<hr.ferit.eventbook.model.Location>()
    private var dayEventLocations = listOf<hr.ferit.eventbook.model.Location>()
    private var dayEvents = listOf<Event>()
    private var markers = mutableListOf<Marker>()
    private var selectedDayOption: DayOptions = DayOptions.TODAY

    private var lastKnownLocation: Location? = null
    private val permissionRequester =
        LocationPermissionRequester(this, ::onPermissionGranted, ::onPermissionDenied)
    private lateinit var buttons: MutableList<Button>
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentEventsMapBinding.inflate(layoutInflater)

        if (savedInstanceState != null) {
            lastKnownLocation = savedInstanceState.getParcelable(KEY_LOCATION)
            cameraPosition = savedInstanceState.getParcelable(KEY_CAMERA_POSITION)
        }

        mapFragment =
            childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        fusedLocationProviderClient =
            LocationServices.getFusedLocationProviderClient(requireContext())

        buttons = mutableListOf(binding.bOption1, binding.bOption2, binding.bOption3)
        setListeners()
        lifecycle.coroutineScope.launch(Dispatchers.IO) {
            getEventsAndLocations()
            filterDayEventLocations()
            lifecycle.coroutineScope.launch(Dispatchers.Main) {
                showCloseEvents()
            }
        }


        return binding.root
    }

    private fun setListeners() {
        binding.bOption1.setOnClickListener {
            selectedDayOption = DayOptions.YESTERDAY
            changeButtonColor(it.id)
            map?.clear()
            markers.clear()
            getEventsForNewDay()
        }
        binding.bOption2.setOnClickListener {
            selectedDayOption = DayOptions.TODAY
            changeButtonColor(it.id)
            map?.clear()
            markers.clear()
            getEventsForNewDay()
        }
        binding.bOption3.setOnClickListener {
            selectedDayOption = DayOptions.TOMORROW
            changeButtonColor(it.id)
            map?.clear()
            markers.clear()
            getEventsForNewDay()
        }
    }


    private fun filterDayEventLocations() {
        val todayEventLocationIds = dayEvents.map { it.locationId?.path?.substringAfterLast("/") }
        dayEventLocations = allLocations.filter { todayEventLocationIds.contains(it.id) }
    }

    private suspend fun getEventsAndLocations() {
        val tasks = listOf(
            lifecycle.coroutineScope.launch(Dispatchers.IO) { collectEvents() },
            lifecycle.coroutineScope.launch(Dispatchers.IO) { collectLocations() }
        )
        tasks.joinAll()
    }

    private fun changeButtonColor(selectedButtonId: Int) {
        for (button in buttons) {
            if (selectedButtonId == button.id) {
                button.setBackgroundColor(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.teal_200
                    )
                )
            } else {
                button.setBackgroundColor(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.gray_unselected_button
                    )
                )
            }
        }

    }

    private fun getEventsForNewDay() {
        lifecycle.coroutineScope.launch(Dispatchers.IO) {
            collectEvents()
            filterDayEventLocations()
            lifecycle.coroutineScope.launch(Dispatchers.Main) {
                showCloseEvents()
            }
        }
    }


    override fun onSaveInstanceState(outState: Bundle) {
        map?.let { map ->
            outState.putParcelable(KEY_CAMERA_POSITION, map.cameraPosition)
            outState.putParcelable(KEY_LOCATION, lastKnownLocation)
        }
        super.onSaveInstanceState(outState)
    }

    override fun onMapReady(map: GoogleMap) {
        map.setInfoWindowAdapter(PopupAdapter(context))
        map.setOnCameraIdleListener(this)
        this.map = map
        permissionRequester.checkPermissions(requireContext())

    }

    private fun onPermissionGranted() {
        locationPermissionGranted = true
        updateLocationUI()
        getDeviceLocation()
    }

    private fun onPermissionDenied() {
        locationPermissionGranted = false
        updateLocationUI()
    }

    private suspend fun collectEvents() {
        dayEvents = when (selectedDayOption) {
            DayOptions.TODAY -> viewModel.getEventsToday().first().getOrDefault(listOf())
            DayOptions.TOMORROW -> viewModel.getEventsTomorrow().first().getOrDefault(listOf())
            DayOptions.YESTERDAY -> viewModel.getEventsYesterday().first().getOrDefault(listOf())
        }
    }

    private suspend fun collectLocations() {
        allLocations = viewModel.getLocations().first().getOrDefault(listOf())
    }

    private fun showCloseEvents() {
        val bounds = map?.projection?.visibleRegion?.latLngBounds
        dayEventLocations.forEach { loc ->
            val latLng = loc.toLatLng()
            if (bounds?.contains(latLng) == true && !markers.any { it.position == latLng }) { //if it's inside viewport and isn't already marked
                var filtered =
                    dayEvents.filter { it.locationId?.path?.substringAfterLast("/") == loc.id }
                var allTitles = filtered.map { it.title }
                val concatStrings = allTitles.joinToString(separator = "\n")

                val marker = map?.addMarker(
                    MarkerOptions()
                        .position(latLng)
                        .title(loc.name)
                        .snippet(concatStrings)
                )
                marker?.let { markers.add(it) }

            }
        }

    }


    private fun zoomIn(location: Location?) {
        if (location != null) {
            val latLng = LatLng(location.latitude, location.longitude)
            map?.animateCamera(CameraUpdateFactory.newLatLng(latLng))
            map?.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, DEFAULT_ZOOM.toFloat()))

        }
    }


    @SuppressLint("MissingPermission")
    private fun getDeviceLocation() {

        val locationResult = fusedLocationProviderClient.lastLocation
        locationResult.addOnCompleteListener(requireActivity()) { task ->
            if (task.isSuccessful) {
                lastKnownLocation = task.result
                if (lastKnownLocation != null) {
                    zoomIn(lastKnownLocation)

                }
            } else {
                defaultLocation.latitude = 45.55111 //set to Osijek if unable to get location
                defaultLocation.longitude = 18.69389
                zoomIn(defaultLocation)
                map?.uiSettings?.isMyLocationButtonEnabled = false
            }
        }


    }

    @SuppressLint("MissingPermission")
    private fun updateLocationUI() {
        if (map == null) {
            return
        }

        if (locationPermissionGranted) {
            map?.isMyLocationEnabled = true
            map?.uiSettings?.isMyLocationButtonEnabled = true
        } else {
            map?.isMyLocationEnabled = false
            map?.uiSettings?.isMyLocationButtonEnabled = false
            lastKnownLocation = null
        }

    }

    override fun onCameraIdle() {
        markers.forEach {
            if (!markerInsideViewPort(it)) {
                it.remove()
            }
        }
        markers = markers.filter {
            markerInsideViewPort(it)
        } as MutableList<Marker>
        showCloseEvents()
    }

    private fun markerInsideViewPort(marker: Marker): Boolean {
        val bounds = map?.projection?.visibleRegion?.latLngBounds
        if (bounds != null) return bounds.contains(marker.position) else return false
    }

    companion object {
        private val TAG = EventsMapFragment::class.java.simpleName
        private const val DEFAULT_ZOOM = 15

        private const val KEY_CAMERA_POSITION = "camera_position"
        private const val KEY_LOCATION = "location"

    }

}