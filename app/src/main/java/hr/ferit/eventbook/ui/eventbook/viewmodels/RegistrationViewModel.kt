package hr.ferit.eventbook.ui.eventbook.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import hr.ferit.eventbook.data.AuthRepository
import hr.ferit.eventbook.data.UserRepository
import hr.ferit.eventbook.model.User
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class RegistrationViewModel(
    private val repo: AuthRepository, private val userRepo: UserRepository
) : ViewModel() {
    val signUpResponse = MutableLiveData<Result<Boolean>>()


    fun signUpWithEmailAndPassword(
        firstName: String,
        lastName: String,
        email: String,
        password: String
    ) = viewModelScope.launch(Dispatchers.IO) {
        var result = repo.firebaseSignUpWithEmailAndPassword(email, password)
        userRepo.addUserToFirestore(User(repo.currentUser?.uid, firstName, lastName, email))
        viewModelScope.launch(Dispatchers.Main) { signUpResponse.value = result }
    }

}