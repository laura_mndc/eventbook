package hr.ferit.eventbook.ui.eventbook.event_list

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.appcompat.widget.SearchView
import androidx.core.view.MenuHost
import androidx.core.view.MenuProvider
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.coroutineScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import hr.ferit.eventbook.R
import hr.ferit.eventbook.databinding.FragmentEventListBinding
import hr.ferit.eventbook.ui.eventbook.viewmodels.AuthViewModel
import hr.ferit.eventbook.ui.eventbook.viewmodels.EventListViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*


class EventListFragment : Fragment(), OnEventEventListener {

    private lateinit var binding: FragmentEventListBinding
    private lateinit var adapter: EventAdapter
    private val viewModel: EventListViewModel by viewModel()
    private val authViewModel: AuthViewModel by viewModel()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentEventListBinding.inflate(layoutInflater)
        setMenu()
        binding.buttonAddEvent.setOnClickListener { showCreateNewEventFragment() }
        binding.bMap.setOnClickListener { showEventMapFragment() }
        setupRecyclerView()

        lifecycle.coroutineScope.launch(Dispatchers.IO) {
            collectEvents()
        }
        lifecycle.coroutineScope.launch(Dispatchers.IO) {
            collectLocations()
        }
        binding.svSearch.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            private var timer: Timer = Timer()
            private val DELAY: Long = 700 // ms

            override fun onQueryTextChange(newText: String?): Boolean {
                timer.cancel()
                timer = Timer()
                timer.schedule(
                    object : TimerTask() {
                        override fun run() {
                            if (newText != null) {
                                if (newText.isNotEmpty()) {
                                    lifecycle.coroutineScope.launch(Dispatchers.IO) {
                                        collectFilteredEvents(newText)
                                    }
                                } else {
                                    lifecycle.coroutineScope.launch(Dispatchers.Main) { adapter.resetEvents() }
                                }
                            }
                        }
                    },
                    DELAY
                )

                return false
            }

        })

        return binding.root
    }

    private fun setMenu() {
        val menuHost: MenuHost = requireActivity()
        menuHost.addMenuProvider(object : MenuProvider {
            override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {

                menuInflater.inflate(R.menu.menu, menu)
            }

            override fun onMenuItemSelected(menuItem: MenuItem): Boolean {

                return when (menuItem.itemId) {
                    R.id.action_logout -> {
                        authViewModel.signOut()
                        showLoginFragment()

                        true
                    }

                    else -> false
                }
            }
        }, viewLifecycleOwner, Lifecycle.State.RESUMED)
    }


    private suspend fun collectFilteredEvents(query: String) {
        viewModel.getFilteredEvents(query).collect() {
            when {
                it.isSuccess -> {
                    lifecycle.coroutineScope.launch(Dispatchers.Main) {
                        adapter.setFilteredEvents(
                            it.getOrDefault(
                                listOf()
                            )
                        )
                    }
                }
                it.isFailure -> {
                    it.exceptionOrNull()?.printStackTrace()
                }
            }

        }
    }


    private suspend fun collectEvents() {
        viewModel.getEvents().collect() {
            when {
                it.isSuccess -> {
                    lifecycle.coroutineScope.launch(Dispatchers.Main) {
                        adapter.setEvents(
                            it.getOrDefault(
                                listOf()
                            )
                        )
                    }
                }
                it.isFailure -> {
                    it.exceptionOrNull()?.printStackTrace()
                }
            }

        }
    }

    private suspend fun collectLocations() {
        viewModel.getLocations().collect() {
            when {
                it.isSuccess -> {
                    lifecycle.coroutineScope.launch(Dispatchers.Main) {
                        adapter.setLocations(
                            it.getOrDefault(
                                listOf()
                            )
                        )
                    }
                }
                it.isFailure -> {
                    it.exceptionOrNull()?.printStackTrace()
                }
            }

        }
    }


    private fun setupRecyclerView() {
        binding.rvEventList.layoutManager = LinearLayoutManager(
            context,
            LinearLayoutManager.VERTICAL,
            false
        )
        adapter = EventAdapter()
        adapter.onEventSelectedListener = this
        binding.rvEventList.adapter = adapter
    }

    companion object {
        val Tag = "EventsList"

        fun create(): Fragment {
            return EventListFragment()
        }
    }

    override fun onEventSelected(id: String?) {
        val action =
            EventListFragmentDirections.actionEventListFragmentToEventDetailsFragment(id ?: "")
        findNavController().navigate(action)
    }


    private fun showCreateNewEventFragment() {
        val action = EventListFragmentDirections.actionEventListFragmentToEventNewFragment()
        findNavController().navigate(action)
    }

    private fun showEventMapFragment() {
        val action = EventListFragmentDirections.actionEventListFragmentToEventsMapFragment()
        findNavController().navigate(action)
    }

    private fun showLoginFragment() {
        val action = EventListFragmentDirections.actionEventListFragmentToLoginFragment()
        findNavController().navigate(action)
    }
}