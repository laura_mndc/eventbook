package hr.ferit.eventbook.ui.eventbook.viewmodels

import android.net.Uri
import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.DocumentReference
import hr.ferit.eventbook.data.AuthRepository
import hr.ferit.eventbook.data.EventRepository
import hr.ferit.eventbook.data.StorageRepository

class EventDetailsViewModel(
    val eventRepository: EventRepository,
    val storageRepository: StorageRepository,
    val authRepository: AuthRepository
) : ViewModel() {

    fun getEventById(id: String) = eventRepository.getEventDetailsById(id)
    fun getLocationByRef(ref: DocumentReference) = eventRepository.getLocationByReference(ref)

    fun getEventImages(id: String) = eventRepository.getEventImagesFromFirestore(id)

    suspend fun save(eventId: String, image: ByteArray) {
        var downloadUrl = storageRepository.addImageToFirebaseStorage(image)
        eventRepository.addPhotoToFirestore(
            authRepository.currentUser?.uid,
            eventId,
            downloadUrl.getOrNull()
        )
    }

    suspend fun save(eventId: String, image: Uri) {
        var downloadUrl = storageRepository.addImageToFirebaseStorage(image)
        eventRepository.addPhotoToFirestore(
            authRepository.currentUser?.uid,
            eventId,
            downloadUrl.getOrNull()
        )
    }
}