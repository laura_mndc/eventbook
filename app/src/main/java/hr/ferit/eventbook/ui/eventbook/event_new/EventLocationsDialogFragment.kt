package hr.ferit.eventbook.ui.eventbook.event_new

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.AutocompleteSessionToken
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.model.RectangularBounds
import com.google.android.libraries.places.api.net.*
import com.google.firebase.firestore.GeoPoint
import hr.ferit.eventbook.R
import hr.ferit.eventbook.databinding.FragmentEventLocationsDialogBinding
import hr.ferit.eventbook.model.AutocompletePrediction
import hr.ferit.eventbook.model.Location
import hr.ferit.eventbook.ui.eventbook.viewmodels.EventNewViewModel
import hr.ferit.eventbook.utils.LocationPermissionRequester
import org.koin.androidx.viewmodel.ext.android.activityViewModel
import java.util.*
import kotlin.math.PI
import kotlin.math.cos


class EventLocationsDialogFragment : DialogFragment(), OnLocationEventListener {
    private lateinit var binding: FragmentEventLocationsDialogBinding
    private lateinit var adapter: LocationAdapter
    private val viewModel: EventNewViewModel by activityViewModel()
    private lateinit var placesClient: PlacesClient
    private var autocompleteToken: AutocompleteSessionToken? = null
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private val defaultLatLong = LatLng(45.55111, 18.69389) //Osijek, Croatia
    private var lastKnownLocation: android.location.Location? = null
    private val permissionRequester =
        LocationPermissionRequester(this, ::onPermissionGranted, ::onPermissionDenied)

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        binding = FragmentEventLocationsDialogBinding.inflate(layoutInflater)
        setupRecyclerView()

        Places.initialize(
            context,
            resources.getString(R.string.maps_api_key)
        ) //auto-generated string resource
        placesClient = Places.createClient(context)
        fusedLocationProviderClient =
            LocationServices.getFusedLocationProviderClient(requireContext())

        permissionRequester.checkPermissions(requireContext())

        setupListeners()
        return AlertDialog.Builder(requireActivity())
            .setView(binding.root)
            .create()
    }

    private fun setupRecyclerView() {
        binding.rvLocationList.layoutManager = LinearLayoutManager(
            context,
            LinearLayoutManager.VERTICAL,
            false
        )
        adapter = LocationAdapter()
        adapter.onLocationSelectedListener = this
        binding.rvLocationList.adapter = adapter
    }


    private fun onPermissionGranted() {
        getDeviceLocation()
    }

    private fun onPermissionDenied() {}

    @SuppressLint("MissingPermission")
    private fun getDeviceLocation() {

        val locationResult = fusedLocationProviderClient.lastLocation
        locationResult.addOnCompleteListener(requireActivity()) { task ->
            if (task.isSuccessful) {
                lastKnownLocation = task.result
            }
        }


    }

    private fun getBoundsFromPosition(location: android.location.Location?): RectangularBounds {

        //default to Slavonian region
        var latSW = 45.180466925422124
        var longSW = 17.990411148507803
        var latNE = 45.68077489774285
        var longNE = 18.81550578235997
        if (location != null) {
            latSW = location.latitude - 1 //~110.5 km
            longSW = location.longitude - 1 / cos(PI / 180) //~111.32km
            latNE = location.latitude + 1 //~110.5 km
            longNE = location.longitude + 1 / cos(PI / 180) //~111.32km

        }
        return RectangularBounds.newInstance(
            LatLng(latSW, longSW),
            LatLng(latNE, longNE)
        )
    }

    private fun setupListeners() {
        binding.etSearchValue.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) {
            }

            private var timer: Timer = Timer()
            private val DELAY: Long = 700 // ms

            override fun afterTextChanged(s: Editable) {
                timer.cancel()
                timer = Timer()
                timer.schedule(
                    object : TimerTask() {
                        override fun run() {
                            autocompleteToken = AutocompleteSessionToken.newInstance()

                            val bounds = getBoundsFromPosition(lastKnownLocation)
                            val origin = if (lastKnownLocation != null) LatLng(
                                lastKnownLocation!!.latitude,
                                lastKnownLocation!!.longitude
                            ) else defaultLatLong
                            val request =
                                FindAutocompletePredictionsRequest.builder()
                                    .setLocationBias(bounds)
                                    .setOrigin(origin)
                                    .setTypesFilter(listOf(getString(R.string.places_api_establisment_type_filter)))
                                    .setSessionToken(autocompleteToken)
                                    .setQuery(s.toString())
                                    .build()
                            placesClient.findAutocompletePredictions(request)
                                .addOnSuccessListener { response: FindAutocompletePredictionsResponse ->
                                    val sortedPredictions =
                                        response.autocompletePredictions.sortedBy { it.distanceMeters }
                                    val predictions = sortedPredictions.map { prediction ->
                                        AutocompletePrediction(
                                            prediction.placeId,
                                            prediction.getPrimaryText(null).toString(),
                                            prediction.getSecondaryText(null).toString()
                                        )
                                    }
                                    activity?.runOnUiThread {
                                        adapter.setFilteredLocations(predictions)
                                    }
                                }.addOnFailureListener { exception: Exception? ->
                                    if (exception is ApiException) {
                                        exception.printStackTrace()
                                    }
                                }
                        }
                    },
                    DELAY
                )
            }
        })

    }

    override fun onLocationSelected(location: AutocompletePrediction) {

        val placeFields = listOf(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG)
        val request = FetchPlaceRequest.builder(location.placeId, placeFields)
            .setSessionToken(autocompleteToken).build()

        placesClient.fetchPlace(request)
            .addOnSuccessListener { response: FetchPlaceResponse ->
                val place = response.place
                viewModel.select(
                    Location(
                        place.id,
                        place.name,
                        GeoPoint(place.latLng.latitude, place.latLng.longitude)
                    )
                )
                dismiss()

            }.addOnFailureListener { exception: Exception ->
                if (exception is ApiException) {
                    exception.printStackTrace()
                }
            }
    }

    companion object {
        const val TAG = "EventLocationsDialogFragment"
    }

}
