package hr.ferit.eventbook.ui.eventbook.authentication.ui.login


import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.FirebaseAuthInvalidUserException
import hr.ferit.eventbook.R
import hr.ferit.eventbook.databinding.FragmentLoginBinding
import hr.ferit.eventbook.ui.eventbook.viewmodels.AuthViewModel
import hr.ferit.eventbook.ui.eventbook.viewmodels.LoginViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class LoginFragment : Fragment() {
    private lateinit var binding: FragmentLoginBinding
    private val viewModel: LoginViewModel by viewModel()
    private val authViewModel: AuthViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentLoginBinding.inflate(layoutInflater)
        setObservers()
        binding.bLogin.setOnClickListener {
            login()
        }
        binding.tvRegister.setOnClickListener {
            goToRegister()
        }
        binding.tvForgotPassword.setOnClickListener {
            showForgotPasswordDialog()
        }



        return binding.root
    }


    private fun showForgotPasswordDialog() {
        val builder = AlertDialog.Builder(requireContext())
        builder.setTitle(getString(R.string.fragment_login_reset_password_dialog))
        builder.setMessage(getString(R.string.fragment_login_reset_password_description))


        builder.setPositiveButton(R.string.dialog_yes_button) { dialog, _ ->
            val email = binding.etEmail.text.toString().trim()
            val toastText: String = if (email.isNotEmpty()) {
                viewModel.sendPasswordResetEmail(email)
                getString(R.string.fragment_login_toast_reset_email_sent)
            } else {
                getString(R.string.fragment_login_toast_missing_email)
            }

            dialog.dismiss()
            Toast.makeText(
                context,
                toastText, Toast.LENGTH_SHORT
            ).show()

        }

        builder.setNegativeButton(R.string.dialog_no_button) { dialog, _ ->
            dialog.dismiss()
        }
        builder.show()
    }

    private fun setObservers() {
        viewModel.signInResponse.observe(viewLifecycleOwner) { response ->
            when {
                response.isSuccess -> {
                    var isAuthorized = response.getOrNull()
                    if (isAuthorized == true) {
                        if (authViewModel.isEmailVerified) {
                            goToHomeScreen()
                        } else {
                            showEmailVerificationDialog()
                        }
                    } else {
                        Toast.makeText(
                            context,
                            getString(R.string.fragment_login_error),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
                response.isFailure -> {
                    try {
                        var ex = response.exceptionOrNull()
                        if (ex != null) {
                            throw ex
                        }
                    } catch (e: FirebaseAuthInvalidCredentialsException) {
                        Toast.makeText(
                            context,
                            getString(R.string.fragment_login_toast_invalid_credential),
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    } catch (e: FirebaseAuthInvalidUserException) {
                        Toast.makeText(
                            context,
                            getString(R.string.fragment_login_toast_invalid_credential),
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    } catch (e: Exception) {
                        Toast.makeText(
                            context,
                            response.exceptionOrNull()?.message,
                            Toast.LENGTH_SHORT
                        ).show()
                    }


                }
            }
        }
    }

    private fun showEmailVerificationDialog() {
        val builder = AlertDialog.Builder(requireContext())
        builder.setTitle("Email address verification")
        builder.setMessage("Your email address hasn't been verified. Would you like us to resend the email?")


        builder.setPositiveButton(R.string.dialog_yes_button) { dialog, _ ->
            val email = binding.etEmail.text.toString().trim()
            val toastText: String = if (email.isNotEmpty()) {
                authViewModel.sendEmailVerification()
                "Verification email has been sent"
            } else {
                "Enter your email first"
            }

            dialog.dismiss()
            Toast.makeText(
                context,
                toastText, Toast.LENGTH_SHORT
            ).show()

        }

        builder.setNegativeButton(R.string.dialog_no_button) { dialog, _ ->
            dialog.dismiss()
        }
        builder.show()
    }

    private fun login() {
        val email = binding.etEmail.text.toString().trim()
        val password = binding.etPassword.text.toString()
        if (email.isNotEmpty() && password.isNotEmpty()) {
            viewModel.signInWithEmailAndPassword(email, password)
        } else {
            Toast.makeText(context, "All fields are required", Toast.LENGTH_SHORT).show()
        }
    }

    private fun goToRegister() {
        val action = LoginFragmentDirections.actionLoginFragmentToRegistrationFragment()
        findNavController().navigate(action)
    }

    private fun goToHomeScreen() {
        val action = LoginFragmentDirections.actionLoginFragmentToEventListFragment()
        findNavController().navigate(action)
    }


}