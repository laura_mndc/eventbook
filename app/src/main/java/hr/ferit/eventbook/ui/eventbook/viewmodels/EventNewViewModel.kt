package hr.ferit.eventbook.ui.eventbook.viewmodels


import android.net.Uri
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import hr.ferit.eventbook.data.AuthRepository
import hr.ferit.eventbook.data.EventRepository
import hr.ferit.eventbook.data.StorageRepository
import hr.ferit.eventbook.model.Location
import java.util.*

class EventNewViewModel(
    val eventRepository: EventRepository,
    val storageRepository: StorageRepository,
    val authRepository: AuthRepository
) : ViewModel() {
    val selectedLocation = MutableLiveData<Location>()

    fun select(item: Location) {
        selectedLocation.value = item
    }

    suspend fun save(title: String, location: Location, dateOfEvent: Date, image: ByteArray) {
        var downloadUrl = storageRepository.addImageToFirebaseStorage(image)
        eventRepository.addEventToFirestore(
            title,
            location,
            dateOfEvent,
            downloadUrl.getOrNull(),
            authRepository.currentUser?.uid
        )
    }

    suspend fun save(title: String, location: Location, dateOfEvent: Date, image: Uri) {
        var downloadUrl = storageRepository.addImageToFirebaseStorage(image)
        eventRepository.addEventToFirestore(
            title,
            location,
            dateOfEvent,
            downloadUrl.getOrNull(),
            authRepository.currentUser?.uid
        )
    }
}