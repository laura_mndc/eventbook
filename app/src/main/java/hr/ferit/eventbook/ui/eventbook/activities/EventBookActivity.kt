package hr.ferit.eventbook.ui.eventbook.activities


import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import hr.ferit.eventbook.databinding.ActivityEventBookBinding


class EventBookActivity : AppCompatActivity() {
    lateinit var binding: ActivityEventBookBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEventBookBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

}