package hr.ferit.eventbook.ui.eventbook.event_details

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import hr.ferit.eventbook.databinding.ItemEventBinding
import hr.ferit.eventbook.databinding.ItemImageBinding
import hr.ferit.eventbook.model.Event
import hr.ferit.eventbook.model.Photo

class EventImageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(image: Photo) {
        val binding = ItemImageBinding.bind(itemView)
        Picasso.get().load(image.uri).into(binding.ivPhoto)
    }


}