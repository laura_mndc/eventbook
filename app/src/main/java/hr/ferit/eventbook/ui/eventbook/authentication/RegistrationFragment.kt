package hr.ferit.eventbook.ui.eventbook.authentication.ui.login


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.FirebaseAuthUserCollisionException
import com.google.firebase.auth.FirebaseAuthWeakPasswordException
import hr.ferit.eventbook.R
import hr.ferit.eventbook.databinding.FragmentRegistrationBinding
import hr.ferit.eventbook.ui.eventbook.viewmodels.AuthViewModel
import hr.ferit.eventbook.ui.eventbook.viewmodels.RegistrationViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class RegistrationFragment : Fragment() {
    private lateinit var binding: FragmentRegistrationBinding
    private val viewModel: RegistrationViewModel by viewModel()
    private val authViewModel: AuthViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentRegistrationBinding.inflate(layoutInflater)
        binding.bRegister.setOnClickListener {
            register()
        }
        binding.tvLogin.setOnClickListener {
            goToLogin()
        }
        setObservers()
        return binding.root
    }

    private fun setObservers() {
        viewModel.signUpResponse.observe(viewLifecycleOwner) { response ->
            when {
                response.isSuccess -> {
                    var isAuthorized = response.getOrNull()
                    if (isAuthorized == true) {
                        authViewModel.sendEmailVerification()
                        goToLogin()
                        Toast.makeText(
                            context,
                            getString(R.string.fragment_register_toast_verify_email),
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        Toast.makeText(
                            context,
                            getString(R.string.fragment_register_error),
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                }
                response.isFailure -> {
                    try {
                        var ex = response.exceptionOrNull()
                        if (ex != null) {
                            throw ex
                        }
                    } catch (e: FirebaseAuthWeakPasswordException) {
                        Toast.makeText(
                            context,
                            getString(R.string.fragment_register_password_length),
                            Toast.LENGTH_SHORT
                        ).show()
                    } catch (e: FirebaseAuthInvalidCredentialsException) {
                        Toast.makeText(
                            context,
                            getString(R.string.fragment_register_email_format),
                            Toast.LENGTH_SHORT
                        ).show()
                    } catch (e: FirebaseAuthUserCollisionException) {
                        Toast.makeText(
                            context,
                            getString(R.string.fragment_register_account_exists),
                            Toast.LENGTH_SHORT
                        ).show()
                    } catch (e: Exception) {
                        Toast.makeText(
                            context,
                            response.exceptionOrNull()?.message,
                            Toast.LENGTH_SHORT
                        ).show()
                    }


                }
            }
        }
    }

    private fun register() {
        val firstName = binding.etFirstName.text.toString().trim()
        val lastName = binding.etLastName.text.toString().trim()
        val email = binding.etEmail.text.toString().trim()
        val password = binding.etPassword.text.toString()
        if (email.isNotEmpty() && password.isNotEmpty() && firstName.isNotEmpty() && lastName.isNotEmpty()) {
            viewModel.signUpWithEmailAndPassword(firstName, lastName, email, password)
        } else {
            Toast.makeText(
                context,
                getString(R.string.fragment_register_required),
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    fun goToLogin() {
        val action = RegistrationFragmentDirections.actionRegistrationFragmentToLoginFragment()
        findNavController().navigate(action)
    }


}