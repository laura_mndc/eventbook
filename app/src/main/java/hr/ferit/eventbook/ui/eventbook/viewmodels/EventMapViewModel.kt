package hr.ferit.eventbook.ui.eventbook.viewmodels

import androidx.lifecycle.ViewModel
import hr.ferit.eventbook.data.EventRepository
import hr.ferit.eventbook.model.Event
import hr.ferit.eventbook.utils.getTodayDate
import kotlinx.coroutines.flow.Flow
import java.util.*

class EventMapViewModel(
    private val eventRepository: EventRepository
) : ViewModel() {
    fun getEventsToday(): Flow<Result<List<Event>>> {
        var endTime = getTodayDate()
        endTime.add(Calendar.DAY_OF_MONTH, 1)
        return eventRepository.getEventsByDateFromFirestore(getTodayDate(), endTime)
    }

    fun getEventsYesterday(): Flow<Result<List<Event>>> {
        var startTime = getTodayDate()
        startTime.add(Calendar.DAY_OF_MONTH, -1)
        return eventRepository.getEventsByDateFromFirestore(startTime, getTodayDate())
    }

    fun getEventsTomorrow(): Flow<Result<List<Event>>> {
        var startTime = getTodayDate()
        startTime.add(Calendar.DAY_OF_MONTH, 1)
        var endTime = getTodayDate()
        endTime.add(Calendar.DAY_OF_MONTH, 2)
        return eventRepository.getEventsByDateFromFirestore(startTime, endTime)
    }

    fun getLocations() = eventRepository.getLocationsFromFirestore()


}