package hr.ferit.eventbook.ui.eventbook.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import hr.ferit.eventbook.data.AuthRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AuthViewModel(
    private val repo: AuthRepository
) : ViewModel() {

    fun signOut() = repo.signOut()
    val isEmailVerified get() = repo.currentUser?.isEmailVerified ?: false

    fun sendEmailVerification() = viewModelScope.launch(Dispatchers.IO) {
        repo.sendEmailVerification()
    }
}