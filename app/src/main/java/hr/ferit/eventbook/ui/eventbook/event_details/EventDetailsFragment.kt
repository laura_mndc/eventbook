package hr.ferit.eventbook.ui.eventbook.event_details

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.coroutineScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.firestore.DocumentReference
import com.squareup.picasso.Picasso
import hr.ferit.eventbook.R
import hr.ferit.eventbook.databinding.FragmentEventDetailsBinding
import hr.ferit.eventbook.model.Event
import hr.ferit.eventbook.model.Location
import hr.ferit.eventbook.ui.eventbook.viewmodels.EventDetailsViewModel
import hr.ferit.eventbook.utils.ImageCompressor
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.text.SimpleDateFormat


class EventDetailsFragment : Fragment(), OnImageEventListener {

    private lateinit var adapter: EventDetailsAdapter
    private lateinit var binding: FragmentEventDetailsBinding
    private val viewModel: EventDetailsViewModel by viewModel()
    private val args: EventDetailsFragmentArgs by navArgs()
    private var location: Location? = null
    private var dateFormat = SimpleDateFormat("dd.MM.yyyy. HH:mm")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentEventDetailsBinding.inflate(layoutInflater)
        setupRecyclerView()
        lifecycle.coroutineScope.launch(Dispatchers.IO) {
            collectPhotos()
        }
        val setResult = registerForActivityResult(ActivityResultContracts.GetMultipleContents())
        { uris ->

            savePhotos(uris)
        }
        binding.bAddImages.setOnClickListener { setResult.launch(getString(R.string.image_intent)) }
        binding.bMap.setOnClickListener { location?.let { loc -> openMaps(loc.toLatLng()) } }
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycle.coroutineScope.launch(Dispatchers.IO) {
            collectAndDisplayEvent()
        }
    }


    private fun openMaps(location: LatLng) {
        val gmmIntentUri =
            Uri.parse("geo:${location.latitude},${location.longitude}?q=${location.latitude},${location.longitude}")
        val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
        mapIntent.setPackage("com.google.android.apps.maps")
        try {
            startActivity(mapIntent)
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(
                context,
                getString(R.string.fragment_details_google_maps_error),
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private suspend fun collectAndDisplayEvent() {
        viewModel.getEventById(args.eventId).collect() {
            when {
                it.isSuccess -> {
                    val event = it.getOrDefault(null)
                    lifecycle.coroutineScope.launch(Dispatchers.Main) { display(event) }
                    getLocationDetails(event?.locationId)
                }
                it.isFailure -> {
                    it.exceptionOrNull()?.printStackTrace()
                }
            }

        }
    }

    private suspend fun getLocationDetails(locationId: DocumentReference?) {
        locationId?.let {
            lifecycle.coroutineScope.launch(Dispatchers.IO) {
                viewModel.getLocationByRef(locationId).collect() {
                    when {
                        it.isSuccess -> {

                            lifecycle.coroutineScope.launch(Dispatchers.Main) {
                                location = it.getOrNull()
                                binding.tvLocation.text = location?.name
                            }
                        }
                        it.isFailure -> {
                            it.exceptionOrNull()?.printStackTrace()
                        }
                    }
                }
            }
        }
    }

    private suspend fun collectPhotos() {
        viewModel.getEventImages(args.eventId).collect() {
            when {
                it.isSuccess -> {
                    lifecycle.coroutineScope.launch(Dispatchers.Main) {
                        adapter.setImages(
                            it.getOrDefault(
                                listOf()
                            )
                        )
                    }
                }
                it.isFailure -> {
                    it.exceptionOrNull()?.printStackTrace()
                }
            }

        }
    }

    private fun display(event: Event?) {
        event?.let {
            binding.apply {
                tvEventTitle.text = event.title
                tvEventDate.text = dateFormat.format(event.dateOfEvent)
                Picasso.get().load(event.coverImageUri).into(binding.ivCoverImage)
            }
        }
    }

    private fun setupRecyclerView() {
        binding.rvEventImages.layoutManager = GridLayoutManager(
            context, 3,
            GridLayoutManager.VERTICAL,
            false
        )
        adapter = EventDetailsAdapter()
        adapter.onImageSelectedListener = this
        binding.rvEventImages.adapter = adapter
    }

    override fun onImageSelected(imageUri: String?) {
        val action =
            EventDetailsFragmentDirections.actionEventDetailsFragmentToEventImageFragment(
                imageUri ?: ""
            )
        findNavController().navigate(action)
    }

    private fun savePhotos(imageUris: List<Uri>) {
        if (imageUris.isNotEmpty()) {

            GlobalScope.launch(Dispatchers.IO) {
                var activity: FragmentActivity? = activity
                var eventId : String? = args.eventId
                for (imageUri in imageUris) {
                    val compressedImage = ImageCompressor.compress(activity, imageUri)
                    if (compressedImage != null) {
                        viewModel.save(eventId!!, compressedImage)
                    } else {
                        viewModel.save(eventId!!, imageUri)
                    }
                }
                GlobalScope.launch(Dispatchers.Main) {
                    Toast.makeText(
                        activity,
                        "Saved images",
                        Toast.LENGTH_SHORT
                    ).show()
                    eventId = null
                    activity =
                        null // variable is now eligible for garbage collection, avoids memory leak
                }
            }

            Toast.makeText(
                context,
                getString(R.string.fragment_details_toast_saving),
                Toast.LENGTH_SHORT
            ).show()
        }


    }


    companion object {
        val Tag = "EventsDetails"
        val EventIdKey = "EventId"

        fun create(id: String): Fragment {
            val fragment = EventDetailsFragment()
            return fragment
        }
    }
}