package hr.ferit.eventbook.ui.eventbook.event_image

import ImageDownloader
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.squareup.picasso.Picasso
import hr.ferit.eventbook.databinding.FragmentEventImageBinding

class EventImageFragment : Fragment() {

    private lateinit var binding: FragmentEventImageBinding
    private val args: EventImageFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentEventImageBinding.inflate(layoutInflater)
        binding.bDownload.setOnClickListener {
            Picasso.get().load(args.imageUri).into(ImageDownloader(context))

        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        display(args.imageUri)
    }

    private fun display(imageUri: String?) {
        imageUri?.let {
            binding.apply {
                Picasso.get().load(imageUri).into(binding.ivPhoto)

            }
        }

    }

    companion object {
        val Tag = "TasksDetails"
        val TaskIdKey = "TaskId"

        fun create(id: String): Fragment {
            val fragment = EventImageFragment()
            return fragment
        }
    }
}