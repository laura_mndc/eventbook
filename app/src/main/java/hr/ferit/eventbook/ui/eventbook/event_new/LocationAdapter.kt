package hr.ferit.eventbook.ui.eventbook.event_new

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import hr.ferit.eventbook.R
import hr.ferit.eventbook.model.AutocompletePrediction

class LocationAdapter : RecyclerView.Adapter<LocationViewHolder>() {

    private var filteredLocations = listOf<AutocompletePrediction>()
    var onLocationSelectedListener: OnLocationEventListener? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LocationViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_location, parent, false)
        return LocationViewHolder(view)
    }

    override fun onBindViewHolder(holder: LocationViewHolder, position: Int) {
        val location = filteredLocations[position]
        holder.bind(location)
        onLocationSelectedListener?.let { listener ->
            holder.itemView.setOnClickListener { listener.onLocationSelected(location) }
        }
    }

    fun setFilteredLocations(locations: List<AutocompletePrediction>) {
        filteredLocations = locations
        this.notifyDataSetChanged()
    }

    override fun getItemCount(): Int = filteredLocations.count()
}