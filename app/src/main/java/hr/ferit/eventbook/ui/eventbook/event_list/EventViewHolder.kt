package hr.ferit.eventbook.ui.eventbook.event_list

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import hr.ferit.eventbook.databinding.ItemEventBinding
import hr.ferit.eventbook.model.Event


class EventViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(event: Event, locationName: String) {
        val binding = ItemEventBinding.bind(itemView)
        binding.itemEventTitle.text = event.title
        binding.itemEventLocation.text = locationName
        Picasso.get().load(event.coverImageUri).into(binding.itemEventImage);

    }


}