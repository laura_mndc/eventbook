package hr.ferit.eventbook.di

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.ktx.storage
import hr.ferit.eventbook.data.*
import hr.ferit.eventbook.ui.eventbook.viewmodels.*

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val databaseModule = module {
    fun provideDatabase(): FirebaseFirestore {
        return Firebase.firestore
    }

    fun provideFirebaseStorage(): FirebaseStorage = Firebase.storage
    fun provideFirebaseAuth(): FirebaseAuth = Firebase.auth
    single { provideDatabase() }
    single { provideFirebaseStorage() }
    single { provideFirebaseAuth() }

}

val repositoryModule = module {
    single<EventRepository> { EventRepositoryImpl(get()) }
    single<StorageRepository> { StorageRepositoryImpl(get()) }
    single<AuthRepository> { AuthRepositoryImpl(get()) }
    single<UserRepository> { UserRepositoryImpl(get()) }
}

val viewmodelModule = module {
    viewModel { EventListViewModel(get()) }
    viewModel { EventDetailsViewModel(get(), get(), get()) }
    viewModel { EventNewViewModel(get(), get(), get()) }
    viewModel { EventMapViewModel(get()) }
    viewModel { AuthViewModel(get()) }
    viewModel { RegistrationViewModel(get(), get()) }
    viewModel { LoginViewModel(get()) }

}


